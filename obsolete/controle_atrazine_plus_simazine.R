# Properties
setwd("/home/slesage/dev/4_contratos/publiceye")
controle_filename <- "prepared_data/controle_full.csv"
population_filename <- "prepared_data/population.csv"
day_output_filename <- "output/controle_atrazine_plus_simazine_day.csv"
month_output_filename <- "output/controle_atrazine_plus_simazine_month.csv"
semester_output_filename <- "output/controle_atrazine_plus_simazine_semester.csv"
year_output_filename <- "output/controle_atrazine_plus_simazine_year.csv"

#These are the two datasets

controle <- read.csv(controle_filename, header=TRUE, sep= ",", dec=".")
colnames(controle)[which(names(controle) == "CODIGO_IBGE")] <- "ibgeCode"

#This is the population data
population <- read.csv(population_filename, header=TRUE, sep= ",", dec=".")

## Functions
toNum <- function(from) {
  if_else(from == "", 0, as.numeric(gsub(",", ".", from) ))
}
controle$result <- toNum(controle$RESULTADO.1)

# Prepare columns
controle$date <- as.Date(controle$DATA_COLETA, "%d/%m/%Y")
controle$day <- format(controle$date, "%d/%m/%Y")
controle$month <- format(controle$date, "%m/%Y")
controle$semester <- ifelse(as.numeric(format(controle$date, "%m")) < 7, format(controle$date, "S1/%Y"), format(controle$date, "S2/%Y"))
controle$year <- format(controle$date, "%Y")

# simazine and atrazine captured on same day
atrazine_plus_simazine_day <- controle %>%
  filter(PARAMETRO == "Simazina" | PARAMETRO == "Atrazina") %>%
  group_by(ibgeCode, day, PARAMETRO) %>%
  summarize(result = max(toNum(result), na.rm=TRUE)) %>%
  group_by(ibgeCode, day) %>%
  summarize(result = sum(result, na.rm=TRUE)) %>%
  group_by(ibgeCode) %>%
  summarize(result = max(result, na.rm=TRUE)) %>%
  group_by(ibgeCode) %>%
  summarise ( ">2ppb" = sum(`result` > 2, na.rm = TRUE),
              "=2ppb" = sum(`result` == 2, na.rm = TRUE),
              "1ppb<x<2ppb" = sum(`result` > 1 & `result` < 2, na.rm = TRUE),
              "=1ppb" = sum(`result` == 1, na.rm = TRUE),
              "0.1ppb<x<1ppb" = sum(`result` > 0.1 & `result` < 1, na.rm = TRUE),
              "=0.1ppb" = sum(`result` == 0.1, na.rm = TRUE),
              "0ppb<x<0.1ppb" = sum(`result` > 0 & `result` < 0.1, na.rm = TRUE),
              "=0ppb" = sum(`result` == "0", na.rm = TRUE),
              "all Detections" = sum(`result` >= 0, na.rm = TRUE),
              "all Measurements" = length(`result`))

#Add the Population number:
atrazine_plus_simazine_day$Population <- 
  population$population[match(atrazine_plus_simazine_day$ibgeCode, population$ibgeCode)]

#Add the State to the Summary Dataset:
atrazine_plus_simazine_day$UF <- 
  population$stateLettersCode[match(atrazine_plus_simazine_day$ibgeCode, population$ibgeCode)]

#Add the Name to the Summary Dataset:
atrazine_plus_simazine_day$name <- 
  population$municipalityName[match(atrazine_plus_simazine_day$ibgeCode, population$ibgeCode)]

#Print all tables in one excel file with multiple sheets
write.csv(atrazine_plus_simazine_day, file = day_output_filename, row.names=FALSE)



# simazine and atrazine captured on same month
atrazine_plus_simazine_month <- controle %>%
  filter(PARAMETRO == "Simazina" | PARAMETRO == "Atrazina") %>%
  group_by(ibgeCode, month, PARAMETRO) %>%
  summarize(result = max(toNum(result), na.rm=TRUE)) %>%
  group_by(ibgeCode, month) %>%
  summarize(result = sum(result, na.rm=TRUE)) %>%
  group_by(ibgeCode) %>%
  summarize(result = max(result, na.rm=TRUE)) %>%
  group_by(ibgeCode) %>%
  summarise ( ">2ppb" = sum(`result` > 2, na.rm = TRUE),
              "=2ppb" = sum(`result` == 2, na.rm = TRUE),
              "1ppb<x<2ppb" = sum(`result` > 1 & `result` < 2, na.rm = TRUE),
              "=1ppb" = sum(`result` == 1, na.rm = TRUE),
              "0.1ppb<x<1ppb" = sum(`result` > 0.1 & `result` < 1, na.rm = TRUE),
              "=0.1ppb" = sum(`result` == 0.1, na.rm = TRUE),
              "0ppb<x<0.1ppb" = sum(`result` > 0 & `result` < 0.1, na.rm = TRUE),
              "=0ppb" = sum(`result` == "0", na.rm = TRUE),
              "all Detections" = sum(`result` >= 0, na.rm = TRUE),
              "all Measurements" = length(`result`))

#Add the Population number:
atrazine_plus_simazine_month$Population <- 
  population$population[match(atrazine_plus_simazine_month$ibgeCode, population$ibgeCode)]

#Add the State to the Summary Dataset:
atrazine_plus_simazine_month$UF <- 
  population$stateLettersCode[match(atrazine_plus_simazine_month$ibgeCode, population$ibgeCode)]

#Add the Name to the Summary Dataset:
atrazine_plus_simazine_month$name <- 
  population$municipalityName[match(atrazine_plus_simazine_month$ibgeCode, population$ibgeCode)]

#Print all tables in one excel file with multiple sheets
write.csv(atrazine_plus_simazine_month, file = month_output_filename, row.names=FALSE)

# simazine and atrazine captured on same semester
atrazine_plus_simazine_semester <- controle %>%
  filter(PARAMETRO == "Simazina" | PARAMETRO == "Atrazina") %>%
  group_by(ibgeCode, semester, PARAMETRO) %>%
  summarize(result = max(toNum(result), na.rm=TRUE)) %>%
  group_by(ibgeCode, semester) %>%
  summarize(result = sum(result, na.rm=TRUE)) %>%
  group_by(ibgeCode) %>%
  summarize(result = max(result, na.rm=TRUE)) %>%
  group_by(ibgeCode) %>%
  summarise ( ">2ppb" = sum(`result` > 2, na.rm = TRUE),
              "=2ppb" = sum(`result` == 2, na.rm = TRUE),
              "1ppb<x<2ppb" = sum(`result` > 1 & `result` < 2, na.rm = TRUE),
              "=1ppb" = sum(`result` == 1, na.rm = TRUE),
              "0.1ppb<x<1ppb" = sum(`result` > 0.1 & `result` < 1, na.rm = TRUE),
              "=0.1ppb" = sum(`result` == 0.1, na.rm = TRUE),
              "0ppb<x<0.1ppb" = sum(`result` > 0 & `result` < 0.1, na.rm = TRUE),
              "=0ppb" = sum(`result` == "0", na.rm = TRUE),
              "all Detections" = sum(`result` >= 0, na.rm = TRUE),
              "all Measurements" = length(`result`))

#Add the Population number:
atrazine_plus_simazine_semester$Population <- 
  population$population[match(atrazine_plus_simazine_semester$ibgeCode, population$ibgeCode)]

#Add the State to the Summary Dataset:
atrazine_plus_simazine_semester$UF <- 
  population$stateLettersCode[match(atrazine_plus_simazine_semester$ibgeCode, population$ibgeCode)]

#Add the Name to the Summary Dataset:
atrazine_plus_simazine_semester$name <- 
  population$municipalityName[match(atrazine_plus_simazine_semester$ibgeCode, population$ibgeCode)]

#Print all tables in one excel file with multiple sheets
write.csv(atrazine_plus_simazine_semester, file = semester_output_filename, row.names=FALSE)

# simazine and atrazine captured on same year
atrazine_plus_simazine_year <- controle %>%
  filter(PARAMETRO == "Simazina" | PARAMETRO == "Atrazina") %>%
  group_by(ibgeCode, year, PARAMETRO) %>%
  summarize(result = max(toNum(result), na.rm=TRUE)) %>%
  group_by(ibgeCode, year) %>%
  summarize(result = sum(result, na.rm=TRUE)) %>%
  group_by(ibgeCode) %>%
  summarize(result = max(result, na.rm=TRUE)) %>%
  group_by(ibgeCode) %>%
  summarise ( ">2ppb" = sum(`result` > 2, na.rm = TRUE),
              "=2ppb" = sum(`result` == 2, na.rm = TRUE),
              "1ppb<x<2ppb" = sum(`result` > 1 & `result` < 2, na.rm = TRUE),
              "=1ppb" = sum(`result` == 1, na.rm = TRUE),
              "0.1ppb<x<1ppb" = sum(`result` > 0.1 & `result` < 1, na.rm = TRUE),
              "=0.1ppb" = sum(`result` == 0.1, na.rm = TRUE),
              "0ppb<x<0.1ppb" = sum(`result` > 0 & `result` < 0.1, na.rm = TRUE),
              "=0ppb" = sum(`result` == "0", na.rm = TRUE),
              "all Detections" = sum(`result` >= 0, na.rm = TRUE),
              "all Measurements" = length(`result`))

#Add the Population number:
atrazine_plus_simazine_year$Population <- 
  population$population[match(atrazine_plus_simazine_year$ibgeCode, population$ibgeCode)]

#Add the State to the Summary Dataset:
atrazine_plus_simazine_year$UF <- 
  population$stateLettersCode[match(atrazine_plus_simazine_year$ibgeCode, population$ibgeCode)]

#Add the Name to the Summary Dataset:
atrazine_plus_simazine_year$name <- 
  population$municipalityName[match(atrazine_plus_simazine_year$ibgeCode, population$ibgeCode)]

#Print all tables in one excel file with multiple sheets
write.csv(atrazine_plus_simazine_year, file = year_output_filename, row.names=FALSE)


# Stats to check
sup2byYear <- sum(atrazine_plus_simazine_year[">2ppb"])
sup2bySemester <- sum(atrazine_plus_simazine_semester[">2ppb"])
sup2byMonth <- sum(atrazine_plus_simazine_month[">2ppb"])
sup2byDay <- sum(atrazine_plus_simazine_day[">2ppb"])


## Test to understand the part of <LD, <LQ and == 0 results in == 0 category, for atrazine


aaa <- controle %>%
  filter(PARAMETRO == "Atrazina")

zero <- controle %>%
  filter((result == 0) & (PARAMETRO == "Atrazina"))

zeroInfLD <- zero %>%
  filter(RESULTADO == "MENOR_LD")

zeroInfLQ <- zero %>%
  filter(RESULTADO == "MENOR_LQ")

zeroOther <- zero %>%
  filter((!(RESULTADO == "MENOR_LQ")) & (!(RESULTADO == "MENOR_LD")))

zeroInfLQ$LQForHistogram <- toNum(zeroInfLQ$LQ)
outliers <- which(zeroInfLQ$LQForHistogram > 2.1)
data <- zeroInfLQ[-outliers, ]
nbreaks <- sqrt(nrow(data))
testHist <- hist(data$LQForHistogram, breaks = nbreaks)

testHist2 <- hist(data$LQForHistogram, breaks = c(seq(0,2,0.1)))



zeroInfLD$LDForHistogram <- toNum(zeroInfLD$LD)
outliers <- which(zeroInfLD$LDForHistogram > 5)
dataLD <- zeroInfLD[-outliers, ]
nbreaks <- sqrt(nrow(dataLD))
testHist <- hist(dataLD$LDForHistogram, breaks = nbreaks)

testHist2 <- hist(dataLD$LDForHistogram, breaks = c(seq(0,5,0.1)))
