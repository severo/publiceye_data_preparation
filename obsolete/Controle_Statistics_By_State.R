# Properties
setwd("/home/slesage/dev/4_contratos/publiceye")
controle_filename <- "prepared_data/controle_full.csv"
population_filename <- "prepared_data/population.csv"
output_byState_filename <- "output/statistics_by_state.csv"
output_atrazine_byState_filename <- "output/statistics_atrazine_by_state.csv"
output_simazine_byState_filename <- "output/statistics_simazine_by_state.csv"
output_simazine_or_atrazine_byState_filename <- "output/statistics_simazine_or_atrazine_by_state.csv"
output_simazine_and_atrazine_byState_filename <- "output/statistics_simazin_and_atrazine_by_state.csv"
tested_byState_filename <- "output/tested_by_state.csv"
tested_withLQ_byState_filename <- "output/tested_withLQ_by_state.csv"

library("tidyr")
library("dplyr")

## Load data
Controle_2014_2017 <- read.csv(controle_filename, header=TRUE, sep= ",", dec=".")
Population_2017_Caps <- read.csv(population_filename, header=TRUE, sep= ",", dec=".")

rm(numMunicipalities, populationGroupedByState, numMunicipalitiesByState, controlsGroupedByMunicipalityAndState, byState)

## Functions
pct <- function(value, total) {
  round(100 * value / total, digits = 2)
}
toNum <- function(from) {
  if_else(from == "", 0, as.numeric(gsub(",", ".", from) ))
}

## Number of municipalities in Brazil
numMunicipalities <- nrow(Population_2017_Caps)

## Number of municipalities by State
populationGroupedByState <- Population_2017_Caps %>% group_by(stateLettersCode)
numMunicipalitiesByState <- populationGroupedByState %>% summarise("total" = n() )
numMunicipalitiesByState$pctOfBrazil <- pct(numMunicipalitiesByState$total, numMunicipalities)
# we check that the sum is coherent
stopifnot(numMunicipalities == sum(numMunicipalitiesByState$total))

## Number of municipalities that never did any test, by state
controlsGroupedByMunicipalityAndState <- Controle_2014_2017 %>% group_by(SIGLA_UF_, CODIGO_IBGE)
# municipalities that did at least one test
byState <- controlsGroupedByMunicipalityAndState %>% 
  summarise(n = first(CODIGO_IBGE)) %>% 
  summarise(atLeastOneTest = n())
byState <- merge(numMunicipalitiesByState, byState, by.x = "stateLettersCode", by.y = "SIGLA_UF_")
byState$atLeastOneTestPct <- pct(byState$atLeastOneTest, byState$total)
# we substract from the total number of municipalities to get the number of municipalities that never did any test
byState$noTest <- byState$total - byState$atLeastOneTest
byState$noTestPct <- pct(byState$noTest, byState$total)

write.csv(byState, file = output_byState_filename, row.names=FALSE)

### Now: only Atrazine

# municipalities that did at least one test
atrazineByState <- controlsGroupedByMunicipalityAndState %>% 
  filter(PARAMETRO == "Atrazina") %>%
  summarise(n = first(CODIGO_IBGE), p = first(PARAMETRO)) %>%
  summarise(atLeastOneTest = n())
atrazineByState <- merge(numMunicipalitiesByState, atrazineByState, by.x = "stateLettersCode", by.y = "SIGLA_UF_")
atrazineByState$atLeastOneTestPct <- pct(atrazineByState$atLeastOneTest, atrazineByState$total)
# we substract from the total number of municipalities to get the number of municipalities that never did any test
atrazineByState$noTest <- atrazineByState$total - atrazineByState$atLeastOneTest
atrazineByState$noTestPct <- pct(atrazineByState$noTest, atrazineByState$total)

write.csv(atrazineByState, file = output_atrazine_byState_filename, row.names=FALSE)


### Now: only Simazine

# municipalities that did at least one test
simazineByState <- controlsGroupedByMunicipalityAndState %>% 
  filter(PARAMETRO == "Simazina") %>%
  summarise(n = first(CODIGO_IBGE), p = first(PARAMETRO)) %>% 
  summarise(atLeastOneTest = n())
simazineByState <- merge(numMunicipalitiesByState, simazineByState, by.x = "stateLettersCode", by.y = "SIGLA_UF_")
simazineByState$atLeastOneTestPct <- pct(simazineByState$atLeastOneTest, simazineByState$total)
# we substract from the total number of municipalities to get the number of municipalities that never did any test
simazineByState$noTest <- simazineByState$total - simazineByState$atLeastOneTest
simazineByState$noTestPct <- pct(simazineByState$noTest, simazineByState$total)

write.csv(simazineByState, file = output_simazine_byState_filename, row.names=FALSE)


### Now: Atrazina or Simazine

# municipalities that did at least one test
simazineOrAtrazinaByState <- controlsGroupedByMunicipalityAndState %>% 
  filter(PARAMETRO == "Simazina" | PARAMETRO == "Atrazina") %>%
  summarise(n = first(CODIGO_IBGE)) %>%
  summarise(atLeastOneTest = n())
simazineOrAtrazinaByState <- merge(numMunicipalitiesByState, simazineOrAtrazinaByState, by.x = "stateLettersCode", by.y = "SIGLA_UF_")
simazineOrAtrazinaByState$atLeastOneTestPct <- pct(simazineOrAtrazinaByState$atLeastOneTest, simazineOrAtrazinaByState$total)
# we substract from the total number of municipalities to get the number of municipalities that never did any test
simazineOrAtrazinaByState$noTest <- simazineOrAtrazinaByState$total - simazineOrAtrazinaByState$atLeastOneTest
simazineOrAtrazinaByState$noTestPct <- pct(simazineOrAtrazinaByState$noTest, simazineOrAtrazinaByState$total)

write.csv(simazineOrAtrazinaByState, file = output_simazine_or_atrazine_byState_filename, row.names=FALSE)

### Now: Atrazina and Simazine

# municipalities that did at least one test
simazineAndAtrazinaByState <- controlsGroupedByMunicipalityAndState %>% 
  filter(PARAMETRO == "Simazina" | PARAMETRO == "Atrazina") %>%
  group_by(SIGLA_UF_, CODIGO_IBGE) %>%
  summarise(num_pest = n_distinct(PARAMETRO)) %>%
  filter(num_pest == 2)  %>%
  summarise(atLeastOneTest = n())
simazineAndAtrazinaByState <- merge(numMunicipalitiesByState, simazineAndAtrazinaByState, by.x = "stateLettersCode", by.y = "SIGLA_UF_")
simazineAndAtrazinaByState$atLeastOneTestPct <- pct(simazineAndAtrazinaByState$atLeastOneTest, simazineAndAtrazinaByState$total)
# we substract from the total number of municipalities to get the number of municipalities that never did any test
simazineAndAtrazinaByState$noTest <- simazineAndAtrazinaByState$total - simazineAndAtrazinaByState$atLeastOneTest
simazineAndAtrazinaByState$noTestPct <- pct(simazineAndAtrazinaByState$noTest, simazineAndAtrazinaByState$total)

write.csv(simazineAndAtrazinaByState, file = output_simazine_and_atrazine_byState_filename, row.names=FALSE)

### Now: in which state: number of tests, and number of detections

Controle_2014_2017$result <- toNum(Controle_2014_2017$RESULTADO.1)

testedByState <- Controle_2014_2017 %>%
  group_by(SIGLA_UF_) %>%
  summarise(tested = n())
# unit test
stopifnot(sum(testedByState$tested) == nrow(Controle_2014_2017))

detectedByState <- Controle_2014_2017 %>%
  filter((TIPO_RESULTADO == "QUANTIFICADO") & (result > 0)) %>%
  group_by(SIGLA_UF_) %>%
  summarise(detected = n())

testedByState <- merge(testedByState, detectedByState)
testedByState$detectedPct <- pct(testedByState$detected, testedByState$tested)

write.csv(testedByState, file = tested_byState_filename, row.names=FALSE)


testedByStateWithLQ <- Controle_2014_2017 %>%
  group_by(SIGLA_UF_) %>%
  summarise(tested = n())
# unit test
stopifnot(sum(testedByState$tested) == nrow(Controle_2014_2017))

detectedByStateWithLQ <- Controle_2014_2017 %>%
  filter((TIPO_RESULTADO == "<LQ") | ((TIPO_RESULTADO == "QUANTIFICADO") & (result > 0))) %>%
  group_by(SIGLA_UF_) %>%
  summarise(detected = n())

testedByStateWithLQ <- merge(testedByStateWithLQ, detectedByStateWithLQ)
testedByStateWithLQ$detectedPct <- pct(testedByStateWithLQ$detected, testedByStateWithLQ$tested)

write.csv(testedByStateWithLQ, file = tested_withLQ_byState_filename, row.names=FALSE)
