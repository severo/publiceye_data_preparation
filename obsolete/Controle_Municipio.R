# Properties
setwd("/home/slesage/dev/4_contratos/publiceye")
controle_filename <- "prepared_data/controle.csv"
population_filename <- "prepared_data/population.csv"
dictionary_filename <- "prepared_data/dictionary.csv"
output_filename <- "output/Controle_Municipio.xlsx"

library("tidyr")
library("dplyr")

#dataset
Controle_2014_2017 <- read.csv(controle_filename, header=TRUE, sep= ",", dec=".")

#This is the population data
Population_2017_Caps <- read.csv(population_filename, header=TRUE, sep= ",", dec=".")

#Dictionary
dictionary <- read.csv(dictionary_filename, header=TRUE, sep= ",", dec=".")

#Here's the number of parametros per Municipio
#First we have to take the subset of the entries, where the parameter has been quantified and the result is bigger than 0
Controle_Municipio <- Controle_2014_2017 %>%
  filter("results" > 0) %>%
  group_by(ibgeCode) %>%
  summarise ("Atrazina" = sum(parameter == dictionary[dictionary["parameter"]=="Atrazina"][1]), 
             "Diuron" = sum(parameter == dictionary[dictionary["parameter"]=="Diuron"][1]),
             "Glifosato + AMPA" = sum(parameter == dictionary[dictionary["parameter"]=="Glifosato + AMPA"][1]),
             "Mancozebe" = sum(parameter == dictionary[dictionary["parameter"]=="Mancozebe"][1]),
             "Profenofós" = sum(parameter == dictionary[dictionary["parameter"]=="Profenofós"][1]),
             "Metolacloro" = sum(parameter == dictionary[dictionary["parameter"]=="Metolacloro"][1]),
             "Simazina" = sum(parameter == dictionary[dictionary["parameter"]=="Simazina"][1]),
             "Alaclor" = sum(parameter == dictionary[dictionary["parameter"]=="Alaclor"][1]),
             "Aldicarbe + Aldicarbesulfona + Aldicarbesulfóxido" = sum(parameter == dictionary[dictionary["parameter"]=="Aldicarbe + Aldicarbesulfona + Aldicarbesulfóxido"][1]),
             "Carbendazim + benomil" = sum(parameter == dictionary[dictionary["parameter"]=="Carbendazim + benomil"][1]),
             "Carbofurano" = sum(parameter == dictionary[dictionary["parameter"]=="Carbofurano"][1]),
             "Clordano" = sum(parameter == dictionary[dictionary["parameter"]=="Clordano"][1]),
             "Clorpirifós + clorpirifós-oxon" = sum(parameter == dictionary[dictionary["parameter"]=="Clorpirifós + clorpirifós-oxon"][1]),
             "DDT + DDD + DDE" = sum(parameter == dictionary[dictionary["parameter"]=="DDT + DDD + DDE"][1]),
             "Endossulfan (a, ß e sais)" = sum(parameter == dictionary[dictionary["parameter"]=="Endossulfan (a, ß e sais)"][1]),
             "Lindano (gama HCH)" = sum(parameter == dictionary[dictionary["parameter"]=="Lindano (gama HCH)"][1]),
             "Metamidofós" = sum(parameter == dictionary[dictionary["parameter"]=="Metamidofós"][1]),
             "Molinato" = sum(parameter == dictionary[dictionary["parameter"]=="Molinato"][1]),
             "Parationa Metílica" = sum(parameter == dictionary[dictionary["parameter"]=="Parationa Metílica"][1]),
             "Pendimentalina" = sum(parameter == dictionary[dictionary["parameter"]=="Pendimentalina"][1]),
             "Permetrina" = sum(parameter == dictionary[dictionary["parameter"]=="Permetrina"][1]),
             "Terbufós" = sum(parameter == dictionary[dictionary["parameter"]=="Terbufós"][1]),
             "Trifluralina" = sum(parameter == dictionary[dictionary["parameter"]=="Trifluralina"][1]),
             "2,4 D + 2,4,5 T" = sum(parameter == dictionary[dictionary["parameter"]=="2,4 D + 2,4,5 T"][1]),
             "Aldrin + Dieldrin" = sum(parameter == dictionary[dictionary["parameter"]=="Aldrin + Dieldrin"][1]),
             "Endrin" = sum(parameter == dictionary[dictionary["parameter"]=="Endrin"][1]),
             "Tebuconazol" = sum(parameter == dictionary[dictionary["parameter"]=="Tebuconazol"][1]))

#Add the Population number:
Controle_Municipio$Population <- 
  Population_2017_Caps$population[match(Controle_Municipio$ibgeCode, Population_2017_Caps$ibgeCode)]

#Add the State to the Summary Dataset:
Controle_Municipio$UF <- 
  Population_2017_Caps$stateLettersCode[match(Controle_Municipio$ibgeCode, Population_2017_Caps$ibgeCode)]

#Add the Name to the Summary Dataset:
Controle_Municipio$name <- 
  Population_2017_Caps$municipalityName[match(Controle_Municipio$ibgeCode, Population_2017_Caps$ibgeCode)]

#print the dataset
library(openxlsx)
write.xlsx(Controle_Municipio, output_filename)

